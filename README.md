# About

This repository is ment to be an easy entry point to clone all the repositories in the group.

To use it you can execute the `clone-all.sh` script and define a `TOKEN` variable containing a token from your Gitlab instance to have access to the script.

```bash
export TOKEN="StringFromGitlab"
./clone-all.sh
```

# Installation

The files of this group must be placed in a path that `LaTeX` can find them. It is recommended to place them within the local texmf tree.

## Manual

Create a directory in your local texmf tree and place the files there. For example, `~/texmf/tex/latex/adn-latex`, where `adn-latex` may be any name you like. And then place all the files from the repository inside that folder.

## Clone repository

Clone this repository into your local texmf tree.

```bash
mkdir -p ~/texmf/tex/latex/
cd ~/texmf/tex/latex/
git clone git@gitlab:adn-latex/adn-latex.git
```

## Clone group

Then you can clone all the repos in the group by executing
```bash
cd adn-latex
export TOKEN="StringFromGitlab"
./clone-all.sh
```
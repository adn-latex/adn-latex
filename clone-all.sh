#!/bin/bash

if [ -z ${TOKEN+x} ]; then 
  echo "TOKEN is unset. Create it in your Gitlab interface, and then create the variable 'TOKEN=<string-from-gitlab>'."
  exit -1
fi

PREFIX="ssh_url_to_repo"

curl --header "PRIVATE-TOKEN: $TOKEN" https://gitlab.com/api/v4/groups/adn-latex/projects | \
grep -o "\"$PREFIX\":[^ ,]\+" | \
awk -F ':' '{for (i=2; i<NF; i++) printf $i ":"; print $NF}' | \
grep --invert-match adn-latex.git | \
xargs -L1 git clone
